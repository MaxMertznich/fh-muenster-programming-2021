import java.util.HashMap;
import java.util.List;

public class Aircraft {

    String registration;        //The legal/federal registration code (unique)
    String type;                //Aircraft type
    int maxTakeoffWeight;       //Maximum Takeoff Weight in Tons
    PerformanceData perf;       //Class which holds the respective performance data
    int takeoffWeight;          //Takeoffweight which is used by calculation
    int emptyWeight = 48;       //Empty weight of the air frame
    int minimumRunwayLength;    //Minimum length needed for takeoff

    public Aircraft(String registration, String type, int maxTakeoffWeight, int minimumRunwayLength){
        this.registration = registration;
        this.type = type;
        this.maxTakeoffWeight = maxTakeoffWeight;
        this.minimumRunwayLength = minimumRunwayLength;
    }

    protected void setTakeoffWeight(int takeoffWeight){
        if(takeoffWeight > emptyWeight && takeoffWeight < maxTakeoffWeight){
            this.takeoffWeight = takeoffWeight;
        } else {
            throw new IllegalArgumentException("Takeoffweight muss zwischen Leergewicht und Maximum Takeoff Weight liegen");
        }
        
    }

    protected void setPerformanceData(HashMap<Integer, List<Integer>> perf){
        this.perf = new PerformanceData(perf);
    }

    protected int getMinimumRunwayLength(){
        return minimumRunwayLength;
    }

    protected int getTakeoffWeight(){
        return takeoffWeight;
    }

    protected int getMaxTakeoffWeight(){
        return maxTakeoffWeight;
    }

    protected int getEmptyWeight(){
        return emptyWeight;
    }
}
