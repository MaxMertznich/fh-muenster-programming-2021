import java.util.HashMap;

public class Airport{

    String icaoCode;
    HashMap<String, Runway> runways;
    Weather airportweather;
    String name;

    public Airport(String icaoCode, String name){
        this.icaoCode = icaoCode;
        this.name = name;
        airportweather = new Weather();
        runways = new CaseInsensitiveHashMap();

    }

    //One Airport can have multiple runways. Thus, runways are stored in a hashmap to let users choose the runway they want. 

    //#region getter and setter methods
/**This is a dummy function to set runways, in this case for Koeln/Bonn (EDDK). For each Runway, a new
 * runway object is created. This can later be called upon. 
 */

    /**Sets the runway at a given airport. As Runways are bi-directional (you usually can use the runway in both directions), arrays are used to insert runways. 
     * @param RunwayString Array of Runway Identifiers
     * @param RunwayLength Array of the Length of the respective Runways
     * @param RunwayHeading Array of the Haadings of the respective Runways
    */
    protected void setRunways(String RunwayString[], int RunwayLength[],  int RunwayHeading[]){
        for(int i = 0; i < RunwayString.length; i++){               //For each runway handed to function
            Runway insertRunway = new Runway();             //Create a new runway for this airport
            insertRunway.setRunwayLength(RunwayLength[i]);
            insertRunway.setRunwayHeading(RunwayHeading[i]);
            insertRunway.setRunwayIdentifier(RunwayString[i]);
            runways.put(RunwayString[i], insertRunway);
            /*Which is inserted into the Runways HashMap with the 
            Identifier as Key (Identifiers are unique, opposed to 
            headings which can be the same for parralel runways)*/
        }
        
    }
    /**This function gathers the current weather for the airport which was either entered via the dummy 
     * function or called via an API
     * @return Weather object which can be accessed for further output
     */
    protected Weather getAirportWeather(){
        return airportweather;
    }
    /**Helper function to enable setting custom weather (e.g. for dummy data insertion purposes) */
    protected void setCustomWeather(int windHeading, int windSpeed){
        airportweather.setWindHeading(windHeading);
        airportweather.setWindSpeed(windSpeed);
    }

    protected String getAirportIdentifier(){
        return icaoCode;
    }

    protected String getAirportName(){
        return name;
    }
}