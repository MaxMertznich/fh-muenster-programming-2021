/*This Polymorph to the HashMap Class allows us to make insert, gets and contains case insensitive,
which is important for user inputs regarding the runways of an airport*/

import java.util.HashMap;

public class CaseInsensitiveHashMap extends HashMap<String,Runway>{

    @Override
    public Runway put(String key, Runway value){
        return super.put(key.toUpperCase(), value);
    }

    @Override
    public Runway get(Object key){
        return super.get(key.toString().toUpperCase());
    }

    @Override
    public boolean containsKey(Object key){
        return super.containsKey(key.toString().toUpperCase());
    }
}