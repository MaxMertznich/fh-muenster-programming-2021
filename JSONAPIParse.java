import java.io.*;               //Used for Input-Stream, core java functionality
import java.net.*;              //Used for URL Connetion
import org.json.JSONObject;    //Used for JSON Functionality, commented out for deliverable and replaced by mockup

public class JSONAPIParse {
    /*This class is used to consolidate calling of APIs and Parsing Responses to JSON*/

    private final String API_KEY;
    private final String API_ADRESS;
    private JSONObject obj;

    public JSONAPIParse(String api_key, String api_adress){
        this.API_KEY = api_key;
        this.API_ADRESS = api_adress;
    }

    /**Class to facilitate data extraction from JSON object via API Call
     * @return JSON Object with requested data*/
    public JSONObject catchAPI(String airport) throws IOException{
        String connectionstring;
        if(API_ADRESS == "https://aerodatabox.p.rapidapi.com/airports/icao/"){
            connectionstring = API_ADRESS + airport + "/runways";                //Specific for runway api
        }
        else{
            connectionstring = API_ADRESS + airport;                             //Specific for weather api
        }     
        
        URLConnection connection = new URL(connectionstring).openConnection(); 
        if(API_ADRESS == "https://aerodatabox.p.rapidapi.com/airports/icao/")
        {
            connection.setRequestProperty("x-rapidapi-key", API_KEY);                           //Authorization header for Runway API
            connection.setRequestProperty("x-rapidapi-host", "aerodatabox.p.rapidapi.com");
        }
        else{
            connection.setRequestProperty("Authorization", API_KEY);                            //Authorization header for weather API
        }
        InputStream response = connection.getInputStream();             //Put Response into inputStream
        if(API_ADRESS != "https://aerodatabox.p.rapidapi.com/airports/icao/"){
        obj = new JSONObject(parseResponse(response));                  //Call function which first parses Response into a string, which in turn is converted to JSON
        }
        return obj;
        /*else{
            try{
            runways = parseResponse(response);     
            runways = "{" + runways.substring(1, runways.length() -1) + "}";
            obj = new JSONObject(runways);             //ONly gets first runway, need more
            } catch(IOException e){
                //
            }
            return obj;
        }*/
    }

    /**Class to parse Response of API call to a String
     * @return String which contains JSON data
     */
    public String parseResponse(InputStream inputStream) throws IOException {               //Cast Answer to string
        BufferedReader bfReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = ""; 
        String result = "";
        while((line = bfReader.readLine()) != null){
            result += line;
        }
        inputStream.close();
        return result;
    }
}