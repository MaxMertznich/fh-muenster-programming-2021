import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import org.junit.Test;

public class JUnitTests {

    @Test (expected = IllegalArgumentException.class) public void RunwayInsertTestMinimumLength(){
        /**Checks if Illegal Exceptions for Runway Insertions work*/
        Runway runway = new Runway();
        runway.setRunwayLength(500);
    }

    @Test (expected = IllegalArgumentException.class) public void RunwayInsertTestHeadingNegative(){
        /**Checks if Illegal Exceptions for negative Headings */
        Runway runway = new Runway();
        runway.setRunwayHeading(-1);
    }

    @Test (expected = IllegalArgumentException.class)
    public void RunwayInsertTestHeadingOverflow(){
        /**Checks if Illegal Exceptions is thrown for headings bigger than 360 */
        Runway runway = new Runway();
        runway.setRunwayHeading(365);
    }

    @Test (expected = IllegalArgumentException.class)
    public void TakeoffWeightInsertTestUpperLimit(){
        Aircraft aircraft = new Aircraft("D-AIDL", "Airbus A320", 77, 1800);
        aircraft.setTakeoffWeight(80);
    }
    @Test (expected = IllegalArgumentException.class)
    public void TakeoffWeightInsertTestLowerLimit(){
        Aircraft aircraft = new Aircraft("D-AIDL", "Airbus A320", 77, 1800);
        aircraft.setTakeoffWeight(45);
    }

    @Test public void setRunwayLength(){
        Runway runway = new Runway();
        runway.setRunwayLength(2500);
        assert(runway.getRunwayLength() == 2500);
    }

    @Test (expected = IllegalArgumentException.class)
    public void setRunwayLengthException(){
        Runway runway = new Runway();
        runway.setRunwayLength(900);
    }

    @Test public void setRunwayHeading(){
        Runway runway = new Runway();
        runway.setRunwayHeading(200);
        assertTrue(runway.getRunwayHeading() == 200);
    }

    @Test (expected = IllegalArgumentException.class)
    public void setRunwayHeadingLowerLimit(){
        Runway runway = new Runway();
        runway.setRunwayHeading(-200);
    }

    @Test (expected = IllegalArgumentException.class)
    public void setRunwayHeadingUpperLimit(){
        Runway runway = new Runway();
        runway.setRunwayHeading(400);
    }

    @Test public void setRunwayIdentifier(){
        Runway runway = new Runway();
        runway.setRunwayIdentifier("20");
        assertTrue(runway.getRunwayIdentifier().equals("20"));
    }

    @Test public void calculateHeadwindComponent(){
        TakeoffPerformance perf = new TakeoffPerformance();
        double hwComponent = perf.calculateHeadwindComponent(40, 62, 10);
        assertTrue(hwComponent == 9.27);
    }

    @Test public void calculateHeadingDifferenceNegative(){
        TakeoffPerformance perf = new TakeoffPerformance();
        assertTrue(perf.headingDifference(40, 330) == -70);
    }

    @Test public void calculateHeadingDifferencePositive(){
        TakeoffPerformance perf = new TakeoffPerformance();
        assertTrue(perf.headingDifference(330, 40) == 70);
    }

    @Test public void setAirportRunways(){
        Airport airport = new Airport("EDDK", "Köln/Bonn");
        String EDDK_runways[] = {"06"};
        int EDDK_runwayLength[] = {2459};
        int EDDK_runwayHeading[] = {63};
        airport.setRunways(EDDK_runways, EDDK_runwayLength, EDDK_runwayHeading);
        assertTrue(airport.runways.get("06").getRunwayHeading() == 63 && airport.runways.get("06").getRunwayIdentifier().equals("06") && airport.runways.get("06").getRunwayLength() == 2459);
    }

    @Test public void CustomWeatherTest(){
        Airport testairport = new Airport("EDDK", "Köln/Bonn");
        testairport.setCustomWeather(220, 5);
        assertTrue(testairport.airportweather.getWindHeading() == 220 && testairport.airportweather.getWindSpeed() == 5);
    }

    @Test public void setAircraftPerformance(){
        Aircraft aircraft = new Aircraft("D-AIDL", "Airbus A320", 77, 1800);
        HashMap<Integer, List<Integer>> performance = new HashMap<Integer, List<Integer>>();
        for (Integer weight = 47, i = 0; weight < 78; weight++, i++) {
            performance.put(weight, Arrays.asList(127+i, 128+i, 129+i));
        }
        aircraft.setPerformanceData(performance);
        aircraft.setTakeoffWeight(62);
        Airport EDDK_airport = new Airport("EDDK", "Köln/Bonn");
        String EDDK_runways[] = {"06", "14L", "14R", "24", "32L", "32R" };
        int EDDK_runwayLength[] = {2459, 3815, 1863, 2459, 1863, 3815 }; 
        int EDDK_runwayHeading[] = {63, 136, 136, 243, 316, 316 }; 
        EDDK_airport.setRunways(EDDK_runways, EDDK_runwayLength, EDDK_runwayHeading); 
        EDDK_airport.setCustomWeather(290,4);
        TakeoffPerformance perf = new TakeoffPerformance();
        List<Integer> result = perf.calculateTakeoffSpeeds(aircraft, EDDK_airport.getAirportWeather(), EDDK_airport.runways.get("14l"));
        assertTrue(result.get(0) == 146 && result.get(1) == 147 && result.get(2) == 148);
    }

    @Test public void CaseInsensitiveHashMap(){
        Airport airport = new Airport("EDDK", "Köln/Bonn");
        String runways[] = {"14L"};
        int runwayLength[] = {3815}; 
        int runwayHeading[] = {136}; 
        airport.setRunways(runways, runwayLength, runwayHeading); 
        assertTrue(airport.runways.containsKey("14L") && airport.runways.containsKey("14l"));
    }
}

