import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Main {

    public static void main(String args[]) {

        // #region Data Insertion
        // #region Airport Init
        Airport EDDK_airport = new Airport("EDDK", "Köln/Bonn"); // Create Airport as base for everything else
        String EDDK_runways[] = {"06", "14L", "14R", "24", "32L", "32R" }; // Create Array with Runway Identifiers
        int EDDK_runwayLength[] = {2459, 3815, 1863, 2459, 1863, 3815 }; // Create Array with Runway Length
        int EDDK_runwayHeading[] = {63, 136, 136, 243, 316, 316 }; // Create Array with Runway Heading
        EDDK_airport.setRunways(EDDK_runways, EDDK_runwayLength, EDDK_runwayHeading); // Insert Runway
        EDDK_airport.setCustomWeather(290,4); //Insert Weather, perspectively, this can be automated via an API request. See commented out code in Weather.java

        Airport EDDL_airport = new Airport("EDDL", "Düsseldorf");
        String EDDL_runways[] = {"05R", "05L", "23R", "23L"};
        int EDDL_runwayLength[] = {3000, 2700, 3000, 2700};
        int EDDL_runwayHeading[] = {52, 52, 232, 232};
        EDDL_airport.setRunways(EDDL_runways, EDDL_runwayLength, EDDL_runwayHeading);
        EDDL_airport.setCustomWeather(270,8);
        Airport[] airportList = {EDDK_airport, EDDL_airport};
        // #endregion
        // #region Aircraft Initiliazation
        Aircraft a320_1 = new Aircraft("D-AIDL", "Airbus A320", 77, 1800);
        Aircraft a321_1 = new Aircraft("D-AISF", "Airbus A321", 77, 2000);

        HashMap<Integer, List<Integer>> performance = new HashMap<Integer, List<Integer>>();
        // #region HashMap Inserts
        // Highly simplified but with raising weight, speeds needed for takeoff
        // increase.
        // This for loop starts at empty weight and stops at max takeoff weight to
        // insert data.

        for (Integer weight = 47, i = 0; weight < 78; weight++, i++) {
            performance.put(weight, Arrays.asList(127+i, 128+i, 129+i));
        }
        // #endregion
        a320_1.setPerformanceData(performance);
        a321_1.setPerformanceData(performance);
        Aircraft[] aircraftList = {a320_1, a321_1};
        // #endregion
        // #endregion

        //Start the UI when data is fully prepared
        new PerformanceCalculator(aircraftList, airportList);
    }
}
