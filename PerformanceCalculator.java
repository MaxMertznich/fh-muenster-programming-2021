import java.util.List;
import java.util.Scanner;

 /** Handles the complete user input to gather data needed and calls the needed functions to complete takeoff calculations
  * @param aircraftList List of all aircrafts stored in database
  * @param airportList  List of all airports stored in database
  * @return Completed Takeoff Calculation
  */ 
public class PerformanceCalculator {

    Aircraft[] aircraftList;
    Aircraft aircraft;
    Airport[] airportList;
    Airport airport;
    Runway runway;
    

    public PerformanceCalculator(Aircraft[] aircraftList, Airport[] airportList) {
        this.aircraftList = aircraftList;
        this.airportList = airportList;

        //Initialise variables for UI management
        boolean aircraftChosen, airportChosen, runwayChosen, takeoffWeightEntered;
        aircraftChosen = airportChosen = runwayChosen = takeoffWeightEntered =  false;

        //Initialise Scanner to get user input
        Scanner input = new Scanner(System.in);
        System.out.println("==========");
        System.out.println("Welcome to the Aircraft Takeoff Performance Editor. First, please choose your aircraft. To do so, please enter the number in front of the aircraft in the list.");
        System.out.println("==========");
        for(int i = 0; i < this.aircraftList.length; i++) {
            //Output Number for User Interface differs from Array enumeration
            Integer outputNumber = i + 1;
            System.out.println(outputNumber + ". " + this.aircraftList[i].registration + " Type: " + this.aircraftList[i].type);
        }
        System.out.println("==========");
        //Using a while loop to check for correctness of user input and trying again if input is not correct.
        while (!aircraftChosen) {
            Integer userChoice;
            try {
                userChoice = input.nextInt();
            } catch (Exception e) {
                throw new IllegalArgumentException("Input Type nicht erlaubt. Bitte nur Zahlen eingeben.");
            }
            if (userChoice > this.aircraftList.length || userChoice < 0) {
                System.out.println("Diese Angabe ist ungültig. Bitte probiere es noch einmal.");
            } else {
                aircraft = this.aircraftList[userChoice-1];
                aircraftChosen = !aircraftChosen;
            }
        }
        System.out.println("==========");
        System.out.println("Weiter geht es zum Flughafen. Bitte wähle einen Flughafen aus der Liste, indem du die jeweilige Nummer eingibst.");
        System.out.println("==========");
        for(int i = 0; i < this.airportList.length; i++){
            Integer outputNumber = i + 1;
            System.out.println(outputNumber + ". " + this.airportList[i].icaoCode + " - " + this.airportList[i].name);
        }
        System.out.println("==========");
        //Using a while loop to check for correctness of user input and trying again if input is not correct.
        while(!airportChosen){
            Integer userChoice;
            try {
                userChoice = input.nextInt();
            } catch (Exception e) {
                throw new IllegalArgumentException("Input Type nicht erlaubt. Bitte nur Zahlen eingeben.");
            }
            if(userChoice > this.airportList.length || userChoice < 0){
                System.out.println("Diese Angabe ist ungültig. Bitte probiere es noch einmal.");
            } else {
                airport = this.airportList[userChoice-1];
                airportChosen = !airportChosen;
            }
        }
        System.out.println("==========");
        System.out.println("Alles klar. Du hast das Flugzeug " + this.aircraft.registration + " und den Flughafen " + this.airport.icaoCode + " gewählt.");
        System.out.println("==========");
        this.airport.getAirportWeather();

        System.out.println("==========");
        System.out.println("Bitte wähle die Startbahn, auf der du starten möchtest:");
        System.out.println("==========");
        for(String key : airport.runways.keySet()){
            System.out.println(key + " - Length: " + airport.runways.get(key).getRunwayLength() + "m");
        }
        System.out.println("==========");
        System.out.println("Gebe dafür einfach einen Identifier aus der Liste ein.");
        //Using a while loop to check for correctness of user input and trying again if input is not correct.
        while(!runwayChosen){
            //Needed to circumvent input reader behaviour which lead to two enters needed to confirm
            if(input.hasNextLine()){
                input.nextLine();
            }
            String userChoice = input.nextLine();
            //Check if user input is contained in keyset
            if(airport.runways.containsKey(userChoice)){
                //Set Runway as user choice
                runway = airport.runways.get(userChoice);
                runwayChosen = !runwayChosen;
                break;
            } else {
                //Repeat
                System.out.println("Diese Startbahn ist nicht vorhanden. Bitte gebe eine passende Startbahn ein.");
            }
        }
        System.out.println("==========");
        System.out.println("Bitte gebe nun dein Startgewicht in ganzen Tonnen ein. Der Wert muss zwischen " + aircraft.getEmptyWeight() + " Tonnen und " + aircraft.getMaxTakeoffWeight() + " Tonnen liegen.");
        System.out.println("==========");
        while(!takeoffWeightEntered){
            int userChoice = input.nextInt();
            if(userChoice >= aircraft.getEmptyWeight() && userChoice <= aircraft.getMaxTakeoffWeight()){
                //Set TakeoffWeight
                aircraft.setTakeoffWeight(userChoice);
                takeoffWeightEntered = !takeoffWeightEntered;
                break;
            } else {
                //Repeat
                System.out.println("Dein eingegebenes Takeoffweight lag außerhalb des erlaubten Rahmens. Bitte probiere es erneut.");
            }
        }

        System.out.println("Bitte warte kurz, bis die Berechnung abgeschlossen ist.");
        TakeoffPerformance toPerf = new TakeoffPerformance();
        List<Integer> takeoffSpeeds = toPerf.calculateTakeoffSpeeds(aircraft, airport.getAirportWeather(), runway);

        // Create Takeoff Report with entered Data and Results
        System.out.println("==========");
        System.out.println("Hier kommt deine Takeoff-Performance:");
        System.out.println("==========");
        System.out.println("Flugzeug: " + aircraft.registration + " - " + aircraft.type);
        System.out.println("Abflug: " + airport.icaoCode + " (" + airport.name + ") - " + runway.getRunwayIdentifier()); 
        System.out.println("Wind: " + airport.getAirportWeather().getWindHeading() + "/" + airport.getAirportWeather().getWindSpeed());
        System.out.println("==========");
        System.out.println("Deine Takeoff-Speeds:");
        System.out.println("V1: " + takeoffSpeeds.get(0));
        System.out.println("VR: " + takeoffSpeeds.get(1));
        System.out.println("V2: " + takeoffSpeeds.get(2));
        input.close();
    }
}
