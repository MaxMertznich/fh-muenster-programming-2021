import java.util.HashMap;
import java.util.List;

public class PerformanceData {

/* The Hashmap saves the Takeoff Speeds for every ton of takeoff weight.*/

    HashMap<Integer, List<Integer>> perf = new HashMap<Integer, List<Integer>>();
    
    public PerformanceData(HashMap<Integer, List<Integer>> performance){
        this.perf = performance;
    }

    protected List<Integer> getPerformanceData(Integer toWeight){
        //Get Array with Speeds based on Takeoff Weight
        return perf.get(toWeight);
    }
}
