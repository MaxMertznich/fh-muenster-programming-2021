public class Runway {
    
    private int runwayHeading;
    private String runwayIdentifier;
    private int runwayLength;
    public static final int systemMinimumRunwayLength = 1000;  //

    public Runway(){
    }

//#region Getter and Setter Methods

    protected void setRunwayHeading(int runwayHeading){
        //Error handling for illegal arguments
        if(runwayHeading > 0 && runwayHeading < 360) {
            this.runwayHeading = runwayHeading;
        } else {
            throw new IllegalArgumentException("Das Heading von Startbahnen muss zwischen 0 und 360 liegen");
        }
    }

    protected void setRunwayIdentifier(String runwayIdentifier){
        this.runwayIdentifier = runwayIdentifier;
    }

    protected void setRunwayLength(int runwayLength){
        //Error handling for illegal arguments
        if(runwayLength < systemMinimumRunwayLength){
            throw new IllegalArgumentException("Die minimale Länge für Startbahnen beträgt " + systemMinimumRunwayLength + " Meter!");
        } else {
            this.runwayLength = runwayLength;
        }
    }

    protected int getRunwayHeading(){
        return runwayHeading;
    }

    protected int getRunwayLength(){
        return runwayLength;
    }

    protected String getRunwayIdentifier(){
    return runwayIdentifier;
    }
    //#endregion
}