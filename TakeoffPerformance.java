import java.lang.Math;
import java.util.List;

public class TakeoffPerformance{

    public TakeoffPerformance(){}

    /**
     * Handles the complete calculation of takeoff speeds
     * @param aircraft  The aircraft used for calculation
     * @param weather   The weather impacting the calculation
     * @param runway    Runway chosen for calculation
     * @return  List with three takeoff speeds (V1, VR, V2)
     */
    protected List<Integer> calculateTakeoffSpeeds(Aircraft aircraft, Weather weather, Runway runway){
        if(runway.getRunwayLength() > aircraft.getMinimumRunwayLength()){
            //Calculating the HeadWind Component and casting it to an Integer for further use
            int headwind = (int) Math.floor(calculateHeadwindComponent(runway.getRunwayHeading(), weather.getWindHeading(), weather.getWindSpeed()));
            List<Integer> takeoffspeeds = aircraft.perf.getPerformanceData(aircraft.takeoffWeight);
            for(int i = 0; i<takeoffspeeds.size(); i++){
                takeoffspeeds.set(i, takeoffspeeds.get(i) - headwind);
            }
            return takeoffspeeds;
        } else {
            throw new IllegalArgumentException("Die minimale Startbahnlänge für dieses Flugzeug wurde nicht erreicht");
        }
    }

    /**
     * Calculates the Headwind Component for a given Runway and Weather combination. This is later used to adjust takeoff speeds accordingly.
     * @param runwayHeading The Heading of the runway the user has chosen
     * @param windHeading The direction of the wind
     * @param windSpeed The speed of the wind
     * @return The calculated headwind component
     */

    protected double calculateHeadwindComponent(int runwayHeading, int windHeading, int windSpeed){
        double cosinusRadian = Math.toRadians(headingDifference(runwayHeading, windHeading));
        double headwindComponent = windSpeed * Math.cos(cosinusRadian);
        return Math.round(headwindComponent * 100.0) / 100.0; //round to two digits after decimal
    }
/**
 * Calculates the difference between the Runway Heading and the Windheading, taking into account the properties of headings (measured 0-360 degrees).
 * @param runwayHeading The Heading of the Runway chosen
 * @param windHeading The Heading the wind is coming from
 * @return returns the difference between both headings.
 */
    protected int headingDifference(int runwayHeading, int windHeading){
       return (windHeading - runwayHeading + 540) % 360 - 180;
    }
}