// import java.io.*;               //Used for Input-Stream, core java functionality

// import org.json.JSONException;
// import org.json.JSONObject;    //Used for JSON Functionality, commented out for deliverable and replaced by mockup

public class Weather {

    private int windHeading;
    private int windSpeed;
    // private JSONObject jsonMetar = new JSONObject();
    // private final static String API_KEY = "4uLiSGNVNbC8ro1TxJGEHSVOTb1Y2ZtuXxtIdwRiGn4";            //max 4k calls/day
    // private final static String API_URL = "http://avwx.rest/api/metar/";                    

    public Weather(){
        // JSONAPIParse parser = new JSONAPIParse(API_KEY, API_URL);                                   //Create Parser Class which bundles code needed for parsing JSON from API call
        // try{
        //     jsonMetar = parser.catchAPI(this.airport);                                              //Call API and get JSON data
        // } catch (IOException e){
        //     System.out.println(e);
        // }
        // getMetarData(jsonMetar);                                                                	//Extract weather data from JSON
    }

    // /**Class to facilitate data extraction from JSON object
    //  * @return Reads METAR data from JSON object*/
    // public void getMetarData(JSONObject metar){
    //     try{
    //     this.windHeading = metar.getJSONObject("wind_direction").getInt("value"); 
    //     }catch(JSONException e){
    //     this.windHeading = 404;                                                                 //If wind is "VRB" (no int), set 404 as sign for calculation that RWYHDG - 180 shall be used for calculation (worst case scenario)
    //     }             
    //     this.windSpeed = metar.getJSONObject("wind_speed").getInt("value");                     //Get values from Key "Wind Speed"
    //     this.metarString = metar.getString("raw");
    // }

    //#region Get and Set Methods */

    protected void setWindSpeed(int windSpeed){
        if(windSpeed >= 0){
            this.windSpeed = windSpeed;
        } else {
            throw new IllegalArgumentException("Windgeschwindigkeit kann nicht unter 0 sein");
        }
    }

    protected void setWindHeading(int windHeading){
        if(windHeading > 0 && windHeading < 360){
            this.windHeading = windHeading;
        } else {
            throw new IllegalArgumentException("Die Windrichtung muss zwischen 0 und 360 liegen");
        }
    }

    protected int getWindSpeed(){
        return windSpeed;
    }

    protected int getWindHeading(){
        return windHeading;
    }
    //#endregion
}
