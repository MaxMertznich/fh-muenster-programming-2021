# Aircraft Takeoff Performance Calculator

For easier access, this repository is also available via https://gitlab.com/MaxMertznich/fh-muenster-programming-2021.

To clone this repository into your IDE:

    git clone https://gitlab.com/MaxMertznich/fh-muenster-programming-2021.git

## Digibim 2021 Programming Deliverable

The following coding project represents a simple **calculator to calculate the critical takeoff speeds for any aircraft** (as long as data is available). While many factors contribute to these calculation, this application includes the most critical ones.

## Background
Every aircraft takeoff consists of at least three critical speeds: V1, VR and V2. V1 highlights the latest chance to safely abort a takeoff and stop on the ground. If V1 has passed, the airplane must take off. VR or V-Rotate is the speed the pilots needs to start lifting the nose of the airplane up to become airborne. Finally, V2 is the safe climbing speed. If any failures occur after V1, this speed is an orientation for pilots to conduct a safe climbout. 

## Software Architecture
Generally, this application was **designed as open as possible**. Most criticial factors are the same across different aircraft models, so as long as data is available, new aircraft models can be added easily. At the relevant points, infrastructure for the automatic gathering of data via APIs is already existing (e.g. Weather), but due to examination restrictions, it is disabled. 

## User Interface
In the current version, the UI is handled via the console of the IDE. However, the UI can already determine if entries are within plausible ranges and of the correct type.

## Usage
To start, execute main.java. This will fill the execution environment with sample data and initialize the UI. You will be guided through the calculation. In every step, the program will offer you choices which you can choose via keyboard entry. If an entry is out of range, the program is able to recover and repeat the data entry. As soon as all necessary data is entered, the program will commence calculations and provide you with a detailed report, containing the critical takeoff speeds.

## Class Explanation

### Aircraft.java
Contains all data relating to the aircraft itself.
1. Registration
2. Aircraft Type
3. Maximum Takeoff Weight
4. Takeoff Performance - Holds all information regarding the performance
5. Takeoff Weight - As entered in UI
6. Empty Weight 
7. Minimum Runway Length

### Airport.java
Contains all data relating to airports and its runways.
1. ICAO-Code - The unique 4 letter identifier given by the ICAO
2. Name - Public/civil name of airport
3. Airportweather - Class with all data relating to weather
4. Runways - Class relating to Runway data

### PerformanceCalculator.java
Handles the UI. Is passed a list with all airports and aircrafts in database upon initiliazation.
1. AircraftList
2. AirportList

### Runway.java
Holds basic information regarding runways.
1. runwayHeading
2. runwayIdentifier
3. runwayLength
4. systemMinimumRunwayLength - Minimum Length system is designed to handle, **constant**

### TakeoffPerformance.java
Handles calculation and methods related to Takeoff Performance. Initializes empty, arguments are directly passed to *calculateTakeoffSpeeds*, which in turn calls *headingDifference* and *calculateHeadWindComponent*

### Weather.java
This class already has code in place to automate gathering of weather data via API. This function however is disabled in the current version. 
1. windHeading - Magnetic Heading of wind, between 0 and 360
2. windSpeed


## Critical Reflection
This program could work a bit more automated if the usage of external classes/utilities (e.g. to simplify API-calls) would have been allowed. In reality, this program would either be completely based on microservices which provide the necessary data or be built around a SQL/NoSQL database as a backend, providing all the necessary data to the program. 

Contrary to the task given in lecture, **this program does not include any examples for class inheritance**. I felt that this was not needed in this projected and would have bloated up the number of files (even more so if one would stick to the minimum requirement of 5 classes). My use case did not present any viable options to include inheritance.
